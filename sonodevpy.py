# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 12:06:11 2019

@author: acer
"""

import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while True:
    _, frame = cap.read()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
 
    blue = np.array([0, 134,0])
    u_blue = np.array([179,229, 255])
    m = cv2.inRange(hsv, blue, u_blue)
    red = np.array([0,88,0])
    u_red = np.array([178,255,255])
    m2=cv2.inRange (hsv,red, u_red)
    green= np.array([25,73,124])
    u_green = np.array([82,230,215])
    m3=cv2.inRange (hsv,green, u_green)
    result = cv2.bitwise_and(frame, frame, mask=m)
    result2 = cv2.bitwise_and(frame, frame, mask=m2)
    result3 = cv2.bitwise_and(frame, frame, mask=m3)
    result1= cv2.bitwise_and(result,result2,result3)
    cv2.imshow("frame", frame)
    cv2.imshow("result", result1)
    key = cv2.waitKey(100)
    if key == 27:
        break
cap.release()
cv2.destroyAllWindows()
